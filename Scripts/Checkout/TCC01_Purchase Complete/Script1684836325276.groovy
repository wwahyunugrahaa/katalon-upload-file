import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Login/Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('Home Page/div_Swag Labs'))

WebUI.click(findTestObject('Home Page/button_Add to cart'))

WebUI.navigateToUrl('https://www.saucedemo.com/cart.html')

WebUI.verifyElementVisible(findTestObject('Cart Page/span_Your Cart'))

WebUI.verifyElementVisible(findTestObject('Cart Page/button_Checkout'))

WebUI.click(findTestObject('Cart Page/button_Checkout'))

WebUI.verifyElementVisible(findTestObject('Cart Page/input_Checkout Your Information_firstName'))

WebUI.verifyElementVisible(findTestObject('Cart Page/input_Checkout Your Information_lastName'))

WebUI.verifyElementVisible(findTestObject('Cart Page/input_Checkout Your Information_postalCode'))

WebUI.setText(findTestObject('Cart Page/input_Checkout Your Information_firstName'), first_name)

WebUI.setText(findTestObject('Cart Page/input_Checkout Your Information_lastName'), last_name)

WebUI.setText(findTestObject('Cart Page/input_Checkout Your Information_postalCode'), postalcode)

WebUI.setText(findTestObject('Cart Page/input_Checkout Your Information_firstName'), 'wahyu')

WebUI.setText(findTestObject('Cart Page/input_Checkout Your Information_lastName'), 'wahyu')

WebUI.setText(findTestObject('Cart Page/input_Checkout Your Information_postalCode'), '123123')

WebUI.click(findTestObject('Cart Page/input_info_continue'))

WebUI.verifyElementVisible(findTestObject('Cart Page/button_Finish'))

WebUI.click(findTestObject('Cart Page/button_Finish'))

WebUI.closeBrowser()

